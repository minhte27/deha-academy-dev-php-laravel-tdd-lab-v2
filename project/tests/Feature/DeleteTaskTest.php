<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteTaskTest extends TestCase
{
    public function deleteTaskRoute($id)
    {
        return route('tasks.destroy', $id);
    }

    public function getListTaskRoute()
    {
        return route('tasks.index');
    }
    /** @test */
    public function authenticate_user_can_delete_task(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->delete($this->deleteTaskRoute($task->id));

        $this->assertDatabaseMissing('tasks', ['id' => $task->id]);
        $response->assertRedirect($this->getListTaskRoute());
    }

    /** @test */
    public function unauthenticate_user_can_not_delete_task()
    {
        $task = Task::factory()->create();
        $response = $this->delete($this->deleteTaskRoute($task->id));

        $response->assertRedirect('login');
    }
}
