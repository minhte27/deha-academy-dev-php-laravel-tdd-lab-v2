<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateTaskTest extends TestCase
{
    public function updateTaskRoute($id)
    {
        return route('tasks.update', $id);
    }

    public function getListTaskTest()
    {
        return route('tasks.index');
    }

    public function getUpdateViewRoute($id)
    {
        return route('tasks.edit', $id);
    }

    /** @test */
    public function authenticate_user_can_update_task(): void
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name,
            'content' => $this->faker->text
        ];
        $response = $this->put($this->updateTaskRoute($task->id), $dataUpdate);

        $this->assertDatabaseHas('tasks', [
            'name' => $dataUpdate['name'], 'content' => $dataUpdate['content']
        ]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListTaskTest());
    }

    /** @test */
    public function unauthenticate_user_can_not_update_task()
    {
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => $this->faker->name,
            'content' => $this->faker->text
        ];
        $response = $this->put($this->updateTaskRoute($task->id), $dataUpdate);
        $response->assertRedirect('login');
    }


    /** @test */
    public function authenticate_user_can_not_update_task_if_name_field_id_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => '',
            'content' => $this->faker->text
        ];
        $response = $this->put($this->updateTaskRoute($task->id), $dataUpdate);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_view_edit_task_form()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get($this->getUpdateViewRoute($task->id));

        $response->assertViewIs('tasks.edit');
    }

    /** @test */
    public function authenticate_user_can_see_name_required_text_if_validate_error()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => '',
            'content' => $this->faker->text
        ];
        $response = $this->from($this->getUpdateViewRoute($task->id))
        ->put($this->updateTaskRoute($task->id), $dataUpdate);

        $response->assertRedirect($this->getUpdateViewRoute($task->id));
    }

    /** @test */
    public function unauthenticate_user_can_not_see_update_task_form_view()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getUpdateViewRoute($task->id));

        $response->assertRedirect('login');
    }
}
