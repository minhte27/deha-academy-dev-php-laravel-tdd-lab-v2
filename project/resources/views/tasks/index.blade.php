@extends('layouts.app')

@section('content')

    <div class="container">
      <form class="d-flex">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" name="keyword" id="keyword">
        <input type="hidden" id="url" value="{{route('tasks.search')}}">
        <button class="btn btn-outline-success my-2 my-sm-0" type="button" onclick="loadTask()">Search</button>
      </form>
      <div class="row justify-content-center">
        <div class="col-md-12">
          <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Content</th>
                <th scope="col">Feature</th>
              </tr>
            </thead>
            <tbody id="task-table-body">
              @foreach ($tasks as $task)
                <tr>
                    <th scope="row"> {{$task->id}} </th>
                    <td> {{$task->name}} </td>
                    <td> {{$task->content}} </td>
                    <td>
                      <a href="{{route('tasks.edit', $task->id)}}"><button type="submit" class="btn btn-success">Edit</button></a>
                      <form action="{{ route('tasks.destroy', $task->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                    </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
@endsection
@section('footer')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="{{asset('template/js/main.js')}}"></script>
@endsection