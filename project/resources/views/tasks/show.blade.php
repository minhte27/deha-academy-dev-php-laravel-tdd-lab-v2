@extends('layouts.app')

@section('content')
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Content</th>
            <th scope="col">Create At</th>
            <th scope="col">Update At</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row"> {{$task->id}} </th>
                <td> {{$task->name}} </td>
                <td> {{$task->content}} </td>
                <td> {{$task->created_at}} </td>
                <td> {{$task->updated_at}} </td>
            </tr>
        </tbody>
    </table>  
@endsection