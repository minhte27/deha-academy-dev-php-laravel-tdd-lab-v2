document.addEventListener('DOMContentLoaded', () => {
    function loadTask() {
        const keyword = $('#keyword').val();
        const url = $('#url').val();

        $.ajax({
            method: 'GET',
            dataType: 'json',
            url: url,
            data: { keyword: keyword },
            success: function(result) {
                $('#task-table-body').html(result.html);
            },
            error: function(xhr, status, error) {
                console.error('AJAX Error: ', status, error);
            }
        });
    }

    window.loadTask = loadTask;
});