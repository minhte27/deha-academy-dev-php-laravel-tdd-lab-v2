<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateTaskRequest;

class TaskController extends Controller
{
    protected $task;

    public function __construct(Task $task)
    {
        $this->task = $task;   
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tasks = $this->task->all();
        return view('tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateTaskRequest $request)
    {
        $this->task->create($request->all());
        return redirect()->route('tasks.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $task = $this->task->findOrFail($id);
        return view('tasks.show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $task = $this->task->findOrFail($id);
        return view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTaskRequest $request, string $id)
    {
        $task = $this->task->findOrFail($id);
        $task->update($request->all());
        return redirect()->route('tasks.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $task = $this->task->findOrFail($id);
        $task->delete();
        return redirect()->route('tasks.index');
    }

    public function search(Request $request)
    {
        $keyword = $request->keyword;
        $tasks = $this->task->where('name', 'like', "%$keyword%")
        ->orWhere('content', 'like', "%$keyword%")->get();
        $html = view('tasks.result-search', ['tasks' => $tasks])->render();
        return response()->json([
            'html' => $html
        ]);
        //return view('tasks.result-search', compact('tasks'));
    }
}
